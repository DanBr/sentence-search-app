function filterList()
{
    // Declare variables
    var input, filter, ul, li, a, i;
    input = document.getElementById('input');
    filter = input.value.toUpperCase();
    ul = document.getElementById("list");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function readText()
{
    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
    // Great success! All the File APIs are supported.
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
}

function handleFileSelect(evt)
{
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var ul = document.getElementById("list");

        while (ul.firstChild) {
            ul.removeChild(ul.firstChild);
        }   
  
          var lines = e.target.result.split(/[\r\n]+/g);
          var row = 0;
          lines.forEach(element => {
            row++;
            var id = "row_"+row;
            var entry = document.createElement("li");
            var node = document.createElement("a");            
            node.setAttribute("href", "#");
            node.setAttribute("id", id);   
            node.setAttribute("onclick", "SelectFunction(this)");         
            node.innerHTML = element;            
            entry.appendChild(node);
            ul.appendChild(entry);            
          });
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsText(f);
    }
}

function SelectFunction(e){
    var range = document.createRange();
    var selection = window.getSelection();
    range.selectNodeContents(document.getElementById(e.id));

    selection.removeAllRanges();
    selection.addRange(range);
    
    document.execCommand("copy");

    animateSnackBar();

    focusInput();
}

function animateSnackBar(){
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
}

function focusInput(){
    var e = document.getElementById("input");
    e.value = "";
    e.focus();
    e.onkeyup();
}

  